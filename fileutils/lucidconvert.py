#! /usr/bin/env python
""" LUCID to MESQUITE PARSER

This program converts a CSV data file exported from Lucid Builder and
converts it into a basic Nexus format.

Provide the input file name and optionally redirect output to a Nexus file.

Usage: LucidConvert.py MyInfile.csv > MyNexusFile.nex

Note regarding Lucid format conversion: 
	All checkmarks (red, green) are converted to positive states in Nexus
	Also, traits with only ? are coded ?, but if they have another state, 
	   then ? is interpreted as a positive state.
	For states, it uses the categories as state names, and the last field 
	   as the state value. Ecology:Color:Red; Ecology:Color:Blue 
	   becomes character Ecology:Color, with value names Red and Blue = states 1 and 2
	   
Version 1.1 - Get input name from sys.argv and 
              output *only* the NEXUS info via stdout
Version 1.0 - Alpha version, Hardwired file names...

No guarantees that this will not ruin your analyses.
Check your results!!

QUESTIONS: steve <at> practicalcomputing {dot} org
"""

Notes = """
Export from Lucid as CSV...
First line is taxon list.
Next lines have characters and states separated by colon...
Following that, are the states 0-3, etc.
   1 = blue check
   2 = green check
   3 = question mark
   4 = large red check (common misinterpreted)
   5 = small red check (rare misinterpreted)
   6 = grey square (not relevant) use -
   
   Example:
   "Size: Longest axis length (mm) Tentacles not included:3-10",0,0,1,1,0,0,1,1,1,1,1,1,0,1,1,1,1,2,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,3,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
   
   NOTE: This program only handles up to 15 states of any character...
   
 You can do 1&2 in mesquite, but not "3?"....
 For continuous data, the entire matrix must be continuous, so this program uses discrete
 
 To do: possibly just take the 2nd-to-last state of a feature name and not the whole thing... 
 	some of the categories don't have a parent, though...
 
traits   0,1,2,3,0
         0,0,1,2,0
		 1,1,0,1,0
		 and want to get a number corresponding to which trait in the list is checked, not the number...
		 In Nexus, this will become:
		 1(1 3)(1 2)(1 2 3)0
		 In Nexus: (XY) means uncertain AND
		 {XY} means multiple states OR 
		 Presently, treating 1-5 as yes, unless all 3's which = ?
Nexus comments use []

"""

VERSION = "1.1"

#LUCID PARSER?

import csv
import sys
import re
from time import asctime
Delimiter =","

DEBUG      = False
SUPERDEBUG = False

def ListToNexusSet(StateString):
	""" Takes a string with the Lucid states and returns 
		the equivalent Nexus set. For example, the Lucid string
		will have My Species 	 113000000
		indicating that characters 1/2 and possibly 3 are applicable
		If all are 6, use -
		If all are 0, use 0
		If all are 3, use ?
		Otherwise, use the position of the index in {} 
		Because AFAICT, Mesquite will not allow a mix of numbered states and ?,
		  in the case of a mix of check and ?, *THE ? GETS TRANSLATED AS A CHECK*
		You can change that behavior by editing this dictionary below.
	"""
	StateConv ={'0':'0',
				'3': '?',
				'6': '-',
				'1':'1',
				'2':'1',
				'4':'1',
				'5':'1',
				}
	UniqueStates = list(set(StateString))
	if (len(UniqueStates) == 1 and UniqueStates[0] in "036"):# str of a string is OK
			return(StateConv[UniqueStates[0]])
	else:
		# Find the position of all non-zero and non-dash states, 
		# corresponding to which specific character state is positive
		# Convert these to the number of the state, up to hex F for 15
		## NOTE: Fails if more than 15 character states!
		Locations = [hex(1 + m.start())[2:].upper() for m in re.finditer('[12345]',StateString)]
		if Locations:
			if len(Locations) == 1:
				return Locations[0]
			else:
				NexusString =  "{" + " ".join(Locations) + "}"
				return NexusString
		else:
			if '6' in StateString:
				return StateConv['6']
			elif '3' in StateString:
				return StateConv['3']
			else:
				return '0'
				
	# END ListToNexus definition
	
	
if len(sys.argv)<2:
	sys.stderr.write(__doc__)
else:
	FileName = sys.argv[1]
	# FileName = "/Users/haddock/Documents/Ctenophore Lucid Key/0220ctenokey-csv.csv"
	print >> sys.stderr, "Reading from file %s" % FileName
	
	FileHandle = open(FileName,'rU')
	MyStream = csv.reader(FileHandle, delimiter=Delimiter)
	
	
	CommentString = "Created with version %s of LucidConvert on %s from Lucid file %s" %(VERSION, asctime(), FileName)
	
	
	# There is a blank before the first comma, so skip that one...
	# CHECK the output of first and last species exported
	SpeciesList = MyStream.next()[1:]
	CharName = []
	CharDict = {}
	StateDict = {}
	CharList = []
	StateLines=[]
	for Row in MyStream:
		if Row[0]:
			CharName.append(Row[0])
			StateLines.append(Row[1:])
	Dimensions = [len(x.split(":")) for x in CharName]
	Characters = [":".join(x.split(":")[:-1]) for x in CharName]
	# States = [x.split(",") for x in StateLines]
	# print "States",States[1]
	if DEBUG:
		print >> sys.stderr, "StateLines:" , StateLines[0:3]
		print >> sys.stderr,  SpeciesList
		print >> sys.stderr, "Dimensions of names:", Dimensions
	for Ind,Name in enumerate(CharName):
		# print Ind, Dimensions[Ind], Name
		Items = Name.split(":")
		# all but the last character = the name
		# (The last character is the state)
		
		Character = ":".join(Items[:-1])
		if not Character in CharList:
			CharList.append(Character)
		CharDict[Character] = CharDict.get(Character,[]) + [Items[-1]]
		StateDict[Character] = StateDict.get(Character,[]) + [Items[-1]]

	for Item in CharList:
		AllStateList = [" ".join(list(set(x))) for x in StateLines]
		if SUPERDEBUG:
			print >> sys.stderr, "ENTRY: %d" %len(CharDict[Item]), Item , CharDict[Item]
# is the state that should be summed...
			print >> sys.stderr, "Dim Statelines:\t", len(StateLines)
			print >> sys.stderr, "Dim CharList  :\t", len(CharList)
			print >> sys.stderr, "Dim CharName  :\t", len(CharName)
			print >> sys.stderr,"set statelines %d: " %len(AllStateList), AllStateList

	TaxonTraits = {}
	for Ind,FullChar in enumerate(CharName):
		Category = ":".join(FullChar.split(":")[:-1])
		StateList = StateLines[Ind]
		for TaxID,Item in enumerate(StateList):
#			print Item
			TaxonTraits[(TaxID,Category)] = TaxonTraits.get((TaxID,Category),"")  + str(Item)
	# print TaxonTraits.keys()
	
	
	if DEBUG:
		print >> sys.stderr, "CharList", CharList

		for Char in CharList:
			print >> sys.stderr, Char
			for TaxID in range(len(StateList)):
				#print SpeciesList[TaxID],"\t",TaxonTraits[(TaxID,Char)]
				print >> sys.stderr, TaxID, SpeciesList[TaxID], "\t", TaxonTraits[(TaxID,Char)], "\t", ListToNexusSet(TaxonTraits[(TaxID,Char)])

	
	FileHandle.close()

	# DONE READING IN FILE, TIME TO WRITE OUT...

	SpeciesSub = ["'" + x.replace(" ","_") +"'" for x in SpeciesList]
	
	NexusHeader = """#NEXUS
[ %s ]
	BEGIN TAXA;
		TITLE Taxa from %s;
		DIMENSIONS NTAX=%d;
		TAXLABELS
			%s 
		;

	END;
	""" 
	print NexusHeader % (CommentString,FileName,len(SpeciesList), " ".join(SpeciesSub))
	# sub in size and taxa

	NexusCharString =""
	for Num,Item in enumerate(CharList):
		ItemList = ["'" + x.replace(" ","_") +"' " for x in CharDict[Item] ]
		NexusCharString +=  " %d '%s' / 0 %s ,\n" % (Num+1, Item ," ".join(ItemList))
	NexusCharString = NexusCharString[:-1]
	# chop off last character... 

	NexusCharBlock = """BEGIN CHARACTERS;
		TITLE  Character_Matrix from %s;
		DIMENSIONS  NCHAR=%d;
		FORMAT DATATYPE = STANDARD GAP = - MISSING = ? SYMBOLS = "  0 1 2 3 4 5 6 7 8 9 A B C D E F G H J K M N P Q R S T U V W X Y Z";
		CHARSTATELABELS 
			%s 
		; 
	""" 
	print NexusCharBlock % (FileName,len(CharList),NexusCharString)

	### PROBLEM: ZERO STATE IS FIRST STATE IN LUCID...

	TaxonString = ""
	for TaxID in range(len(StateList)):
		TaxonString += "\t%s  " % SpeciesSub[TaxID]
		for Char in CharList:
			TaxonString += ListToNexusSet(TaxonTraits[(TaxID,Char)])
		TaxonString +="\n"
	
	NexusMatrix = """	MATRIX
	%s
	;
	END;
	""" % (TaxonString)

	print NexusMatrix

	print >> sys.stderr, ".................................\n   Found %d traits in %d taxa." % (len(CharList),len(SpeciesList))
	print >> sys.stderr, "   First and last taxa : %s   ||   %s" % (SpeciesList[0],SpeciesList[-1])
	print >> sys.stderr, "   First and last trait: %s   ||   %s" % (CharList[0],CharList[-1])



	
"""EXAMPLE OF SIMPLE NEXUS DATA FORMAT THIS PROGRAM EMULATES:
BEGIN TAXA;
	TITLE Taxa;
	DIMENSIONS NTAX=3;
	TAXLABELS
		taxon_1 taxon_2 taxon_3 
	;

END;

BEGIN CHARACTERS;
	TITLE  Character_Matrix;
	DIMENSIONS  NCHAR=5;
	FORMAT DATATYPE = STANDARD GAP = - MISSING = ? SYMBOLS = "  0 1 2 3 4 5 6 7 8 9 A B C D E F G H J K M N P Q R S T U V W X Y Z a b";
		
	CHARSTATELABELS 
		1 'Body:Lobe' /  one two, 2 tail_color /  blue red ; 
		
	MATRIX
	taxon_1  0a(1 2)13
	taxon_2  10(1 2 3)22
	taxon_3  023(4 b)5

;

"""