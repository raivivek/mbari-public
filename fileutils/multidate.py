#!/usr/bin/env python
""" 
An example of parsing date fields from multiple files
and outputting them as unix epoch seconds (secs since 1/1/1970)

The format of the date field is given in DateFormat string.
This example is for the file:
 http://practicalcomputing.org/examples/ThalassocalyceData.txt
 
You can use these fields (with %) for the format string in
the strptime and strftime methods:
# d = day of month, j = yearday,
# Y = 4-digit year, y = 2-digit year
# m = month, b = short month name, B = full month name
# H = 24 hour hours, h = 12-hour hours, M = minute, S=seconds
# D = shorthand for 'MM/DD/YY' , f for fractional seconds (not kept!)

Usage: 
   multidate.py *.txt > output.dat

where *.txt represents the input files. Don't use .txt as an 
extension for the output file if you use this syntax!

"""

import sys
import time

FileCount = 0

# lines to skip at the top of the file
SkipLines = 1

# designed to work with ThalassocalyceData.txt -- edit for your data
DateFormat = "%Y-%m-%d %H:%M:%S.%f"

for FileName in sys.argv[1:]:
	LineCount = 0
	
	sys.stderr.write("## Processing File: %s\n" % (FileName) )
	
	FileIO = file(FileName,'rU')
	for Row in FileIO:
		LineCount += 1
		if LineCount > SkipLines:
			Fields = Row.rstrip('\n').split('\t')
			
			# Pull from Field number 5, using the DateFormat string
			TimeTuple = time.strptime(Fields[5],DateFormat)
			
			# this is a float, but the fraction is always zero!
			# to keep fractions, chop them off above and add back on
			UnixTime = time.mktime(TimeTuple)
			
			# choose fields to print or other values
			
			print "%s\t%d\t%d" % (FileName,LineCount,UnixTime)
	FileIO.close()
	FileCount += 1

sys.stderr.write("## Processed %d files...\n" % (FileCount) )