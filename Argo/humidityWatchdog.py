#!/opt/python/bin/python
# Pismo specific path for Python 2.7

""" Pull ISUS float data from directory on pismo.shore.mbari.org and make plots
    Must set your home directly with the homeDir variable below, and have
    ~/Argo/data and ~/Argo/plots directories
"""
# normally use
#!/usr/bin/env python

# Parse humidity data from Argo float files and aggregate them

import sys
from glob import glob
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# Set to your home directory
# expects you to have a ~/Argo/data directory and /Argo/plots
homeDir = '/u/mccann'

def collectHum():
	'''
	Pull ISUS can humidity data from all floats
	'''
	highHum = {}
	for floatDir in glob('/mbari/ChemWebData/floats/[0-9][0-9][0-9][0-9]'):
		##print floatDir
		fileName = homeDir + '/Argo/data/' + floatDir.split('/')[-1] + '_combined.dat'
		##print fileName
		outFile = open(fileName,'w')

		isusFiles = glob(floatDir + '/data/*.isus')
		isusFiles.sort()
		for isusFile in isusFiles:
			##print '\t', isusFile
			isusFH = open(isusFile)
			for line in isusFH:
				##print line
				if line.startswith('0x'):
					try:
						esec = line.split(',')[3]
						humi = line.split(',')[12]
						if float(humi) > 40:
							highHum[floatDir] = highHum.get(floatDir,0) + 1

						outFile.write('%s\t%s\n' % (esec, humi))
					except IndexError:
						pass
						##print line
						##sys.exit()
			isusFH.close()
		outFile.close()

	# Report on sinking drifters
	for floatDir in highHum.keys():
		print "I'm sinking and I can't get up: %s, count = %d" % (floatDir, highHum[floatDir])

def plotValues(ax, floatNum, esecList, values):
	'''
	Use Matplotlib to plot humidity time series data
	'''
	datenums = mdates.epoch2num(esecList)
	ax.plot_date(datenums, values, fmt='-', label=floatNum, alpha=0.5)
	ax.set_title('Argo ISUS can humidity')
	ax.set_xlabel('Time (GMT)')
	ax.set_ylabel('RH (%)')


def makePlots():
	'''
	Loop though combined data files and make a plot of the humidity time series
	'''
	fig = plt.figure()
	ax = fig.add_subplot(111)
	humFiles = glob(homeDir + '/Argo/data/*')
	for humFile in humFiles:
		##print humFile
		floatNum = humFile.split('/')[-1].split('_')[0]
		esecList = []
		humList = []
		for line in open(humFile):
			allVals = line.split('\t')
			if float(allVals[0]) != 0:
				esecList.append(float(allVals[0]))
				humList.append(float(allVals[1]))

		plotValues(ax, floatNum, esecList, humList)

	ax.legend(loc="upper left", ncol=3)
	labels = ax.get_xticklabels()
	for label in labels:
		label.set_rotation(30)
	fig.savefig(homeDir + '/Argo/plots/all')
	fig.savefig(homeDir + '/public_html/all')
	print "See plot at http://www2.mbari.org/~%s/all.png" % homeDir.split('/')[-1]

#
# Run the program when executed at the command line
#
if __name__ == '__main__':
	collectHum()
	makePlots()

