#!/usr/bin/env python

usage="""Reads in FASTA or NBRF sequence alignments, with Mac or unix CRs and
outputs (or prints) files with only unique sequences and sites

usage:
	seqlite.py seqname.fta

Note, this is a very immature script and should be used with caution. 

v. 1.2  -- changed to use lists in mybio http://www.mbari.org/staff/haddock/scripts 
v. 1.1 --- fixed major bug where last sequence was omitted (should use mybio to read)
v. 1.07 -- started to add support for sys.arg and skipping duplicate names
(This would probably be better done with the matrix tools in numpy)
"""

import re
import sys
import mybio as mb
#from sets import Set as set     # needed for old python < 2.3?

# INITIALIZE STUFF


### Change settings here
keepall=False # don't chop out conserved sites? just redundant seqs

printtoscreen=True # false means it will save to the outfile
savetofile=False
fractionseqs=2.0 # proportion below which to label (make 4.0 for 25%)

sortthem = False # GET RID OF THIS
skipcount = 0 


#other definitinos
firstseq = False
seqname= ''


# First, load in the seqs into a dictionary
if len(sys.argv)>1:
	inputfilename = sys.argv[1]
else: 
	print >> sys.stderr, usage
	

dataname,datarows = mb.readfasta_list(inputfilename) 

# added
numseqs = len(dataname) # after redundant taxon removal


datacol_list = map(lambda *datarows: [elem or '-' for elem in datarows], *datarows) 

# remove invariable columns by testing for the length of set()
# this function leaves unique values
regsub = re.compile(r'[-?xX]')
labelindex = {}
colnum=0
datacols = []
for ri in range(len(datacol_list)):
	tempcol=(''.join(datacol_list[ri]))
	# Ignore dashes in making consensus
	try: 
		testset=set(regsub.sub('',tempcol))
	except NameError:
		print >> sys.stderr, "failed to find set command"
		print >> sys.stderr,'update to python 2.5 or add the line "from sets import Set as set"'
		printtoscreen=False # false means it will save to the outfile
		printhtml=False
		savetofile=False
		break

	if len(testset)>1 or keepall: # keepall will be the column saver
		datacols.append(tempcol)
		starts=[]
		currentindex=[]
		for lett in testset:
		# should define this as a fraction above
			currentcount=tempcol.count(lett) 
			# print str(colnum) +':'+ lett + ':'+ str(currentcount)
			# ** Can change this to MORE THAN **
			if currentcount < (numseqs/fractionseqs):
				starts = [match.start() for match in re.finditer(lett, tempcol)]
				currentindex.extend(starts)
			# print str(colnum) +':' +lett +':' + str(starts)
		for eyes in set(currentindex):
			try:
				labelindex[eyes].extend([colnum])
			except KeyError:
				labelindex[eyes]=[colnum]
		colnum += 1

## DEBUG ##
# print 'labelindex', labelindex

# return the sequences to their original orientation...
datarow_sub=[]
datarow_list= map(lambda *datacols: [elem or '-' for elem in datacols], *datacols) 

# these are lists, so have to "pack" them back into strings
for ri in range(len(datarow_list)):
	datarow_sub.append(''.join(datarow_list[ri]))

# print datarow_sub

# (create a numbered list that we can use to look up in the index order later)


indexholder = range(numseqs)
# pull names out of original list to add to new list...

indexdict = dict(zip(datarow_sub,indexholder))

# make a dictionary with d['a'][0] = index and d['a'][1] as name

## DEBUG ##
# print 'indexdict:',indexdict

# remove repeats now that the list has been shortened
# for the moment, just keep all rows, even if they are now identical...

if keepall:
	sortkeys = list(datarow_sub)
else:
	sortkeys = list(set(datarow_sub))  # still unsorted

# sort keys (i.e., sequences) to group by similarity
if sortthem:
	sortkeys.sort()
	outdict=dict(zip(datarow_sub,dataname))
	# needed???
	datarow_sub.sort()

	
########### need to check that we aren't messing things up with all this sorting!

origname = dataname
origval  = datarows

maxlen = max([len(v) for v in datarows])

# should remove 1 from the length if NBRF format because of the asterisk
introout='Processing file '+ inputfilename 
print >> sys.stderr,introout
firstout=  "From a list of %d sites in %d sequences," % (maxlen, len(origname))
print >> sys.stderr, firstout
secondout= "%d unique sites remain in %d sequences\n" % (len(datarow_sub[0]), len(sortkeys))
if skipcount:
	secondout += "Skipped " + str(skipcount) + " sequences with duplicate names.\n"
print >> sys.stderr,secondout


if printtoscreen:
	for ind,name in enumerate(dataname):
		print ">" + name
		print datarow_sub[ind]
		# if  indexdict[pri] in labelindex.keys():
		# 	print 'yes', str(labelindex[indexdict[pri]])

# save seqs to file
if savetofile: # not printtoscreen, i.e., print to file
	outFile = file(outfilename, 'w')
	for ind,name in enumerate(dataname):
		labelstring='>'+ name+'\n'
		outFile.write(labelstring)
		outFile.write(mb.stringblock(datarow_sub[ind]))
		outFile.write('\n')
	outFile.close()
	print >> sys.stderr, "Saved to file \'" + outfilename + "\' ...I think"
