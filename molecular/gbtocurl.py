#!/usr/bin/env python
# take a list of genbank tags and generate curl statements 
# to retrieve the protein files (assuming prot = dna +1). 
# version 1.0 - 30 Apr 2008

import re
import os

from sets import Set as set # useful for version 2.3?

# INITIALIZE STUFF
savefile=True

#four files to compare
inputfilename=('allAnimalTags.txt')
# inputfilename=('someAnimalTags.txt')

#name of saved file starts with:
outfileroot='allAnimalprot'
outname=outfileroot+'.sh'

# curl command for the fasta text view of a record.
querystart='''curl "http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&qty=1&c_start=1&list_uids='''

queryend='''&uids=&dopt=fasta&dispmax=5&sendto=t&from=begin&to=end" -m 30 >> allAnimalProt.fta'''


# >gi|155660|gb|M62653.1|AEVGFPA Aequorea victoria green-fluorescent protein mRNA, complete cds

genbank = re.compile(r'^>gi\|([0-9]+)\|.*')
#                                  1number

# search.group(0) is the whole result, not just ()

# FIRST for REPROLYSIN
inFiles= file(inputfilename, 'r')
if savefile:
	outFile = file(outname, 'w')
	outFile.write('#!/bin/bash\n')
for inline in inFiles:
	line=inline.strip('\r\n') 
	# print line
#	m = genbank.search(line)
	if m:
		#genbank protein codes are one more than the nt code.
		newnum=int(m.group(1))+1
		curlname=querystart+str(newnum)+queryend

		## HERE you could probably just do os.command(curlname) instead !!
		
		if savefile:
			outFile.write(curlname)
			outFile.write('\n')
		else:
			print curlname
inFiles.close()

if savefile:
	outFile.close()
	os.system('chmod 775 ' + outname)
	print "Saved to",outname
