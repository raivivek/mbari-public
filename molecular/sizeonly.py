#! /usr/bin/env python

# version 1.1 - cleaned up a bit, used an ordered list
# version 1.0 - first version 


"""
SIZEONLY.PY - version 1.1
  Print out the sizes of the sequences in a file
  
  Requires the mybio.py library from 
  http://www.mbari.org/~haddock/scripts/ 

Usage: 
	sizeonly.py Tr100_filename.fasta > lengths.txt
	
"""
import sys
import mybio as mb

if len(sys.argv)<2:
	print __doc__	

else:
	
	inputfilename = sys.argv[1]
	names,sequences = mb.readfasta_list(inputfilename) 
	# don't append sequences, nor remove dupes, not a quality file but do return an ordered list
	
	sys.stderr.write("Found %d sequences in %s\n" % (len(names), inputfilename))
	for sval in sequences:
		print len(sval)
