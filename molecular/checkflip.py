#! /usr/bin/env python

usage="""
Check that all the sequences in an alignment match the orientation of the first string

Requires the "Jellyfish" python module (not my work!)
  Install with 'sudo easy_install jellyfish'
  
Usage:
  checkflip.py UnflippedAlignment.fta > FlippedSeqs.fta
"""

import sys
import os
# needed for maketrans -- not sure if this is obsolete
import string
import mybio as mb
import jellyfish # fuzzy matching library via easy_install jellyfish

def revcomp(dna):
	#reverse complement of a DNA sequence
	comp = dna.translate(string.maketrans("ATGCatgcRYMKrymkHBVDhbvd", "TACGTACGYRKMYRKMDVBHDVBH"))

	lcomp = list(comp)
	lcomp.reverse()
	return "".join(lcomp)

def fuzzymatch(refsequence, reversedreference, querysequence):
	# better with our without gaps??
	# q = querysequence.upper()
	q = querysequence.replace("-","").upper()
	# options include jaro_distance, damerau_levenshtein_distance, jaro_winkler
#	forwardscore2 = jellyfish.jaro_winkler(q,refsequence)
# 	reversescore2 = jellyfish.jaro_winkler(q,reversedreference)
	
	forwardscore = jellyfish.levenshtein_distance(q,refsequence)
 	reversescore = jellyfish.levenshtein_distance(q,reversedreference)
 	
 	# lower score (number of changes req'd) is better, but allow +- 1 without flipping?
	if (reversescore - forwardscore) >= -1: # lower score is better
#	 	sys.stderr.write("%d\t%d\t%.3f\t%.3f\n" % (forwardscore,reversescore,forwardscore2,reversescore2) )
		if (reversescore - forwardscore) <= 1:
			sys.stderr.write(greencolor)
		sys.stderr.write("%d\t%d\n" % (forwardscore,reversescore) )
		sys.stderr.write(coloroff)
		return querysequence, False
	else:
		sys.stderr.write(redcolor)
		sys.stderr.write(boldon)		
#	 	sys.stderr.write("%d\t%d\t%.3f\t%.3f\n" % (forwardscore,reversescore,forwardscore2,reversescore2) )
		sys.stderr.write("%d\t%d\n" % (forwardscore,reversescore) )
		sys.stderr.write(coloroff)
		return revcomp(querysequence), True


##########################
# START MAIN PROGRAM
skipcount=0
checkfile=0
minsize=1
blockwidth=71
noread=False
sequences={}
ordernames=[]

redcolor   = '\x1b[31m'
greencolor = '\x1b[32m'
boldon     = '\x1b[1m'
boldoff    = '\x1b[22m'
coloroff   = '\x1b[0m'

if len(sys.argv)<2:
	sys.stderr.write(usage)

# optional: add a second argument to tell which sequence to use as the reference.
# optional: do majority rule to find them?

else:
	inputfilename = sys.argv[1]
		
	if checkfile:
		sequences,ordernames = mb.readfasta(inputfilename,False,True,True) # don't append sequences, but *do* remove dupes, and in order
	else:
		sequences,ordernames = mb.readfasta_nocheck(inputfilename,False, False, False,True) # don't append sequences, but *do* remove dupes, and in order

	# create the reference sequences just once, so we don't have to RC each other string.
	refname = ordernames[0]
	refseq = sequences[refname].replace("-","").upper()
	refrc = revcomp(refseq)
	
	assert (refseq.count("A")/float(len(refseq)) > .1), "\nIs this a DNA file or Protein? The program only works on DNA."
	
	outseq = [refseq]
	fliplist = []
	flipcount = 0
	for name in ordernames[1:]:
		sequence=sequences[name]
		newseq = sequence.upper()
		
		# do fuzzy match vs refseq here
		sys.stderr.write(name+"\t")
		correctsequence,flipped = fuzzymatch(refseq,refrc,newseq)
		
		if flipped:
			fliplist.append(name)
			flipcount += 1
		outseq.append(correctsequence)
	
	
	for ind,name in enumerate(ordernames):
		print ">"+name
		print mb.stringblock(outseq[ind]).rstrip()
	sys.stderr.write(redcolor)
	sys.stderr.write(".....................\n")
	sys.stderr.write(coloroff)
	sys.stderr.write("Flipped %d sequences\n" % flipcount)
	sys.stderr.write("\n".join(fliplist))
	sys.stderr.write(redcolor)
	sys.stderr.write("\n.....................\n")
	sys.stderr.write(coloroff)
