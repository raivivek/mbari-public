#! /usr/bin/env python

Usage = """
Merges two SAM counts files into one.
Input format is ###\tName for each file,
and output is Name\t###\t###

Usage mergecounts.py File1 File2 > mergedresults.dat

"""
import sys
if len(sys.argv)<2:
	sys.stderr.write(Usage)
else:
	FirstFile  = sys.argv[1]
	SecondFile = sys.argv[2]
	
	FileObj = file(FirstFile,'rU')
	CountDict = {}
	ReadList = []
	for Line in FileObj:
		Count,Name = Line.rstrip().split('\t')
		
		# pre-load the dictionary with zeroes, in case that 
		# gene is not in the second annotation file
		
		CountDict[Name]= [Count,'0']
		ReadList.append(Name)
	FileObj.close()
	
	SecondObj = file(SecondFile,'rU')
	for Line in SecondObj:
		Count,Name = Line.rstrip().split('\t')
		
		# if there is a record from the first file,
		# add the new value to the list
		
		if Name in ReadList:
			CountDict[Name] = [CountDict[Name][0],Count]
		# otherwise, create a new entry with zero as the first value
		else:
			ReadList.append(Name)
			CountDict[Name] = ['0',Count]

	# entries with [Count,]
	SecondObj.close()
	
	for Item in ReadList:
		print "%s\t%s\t%s" % (Item, CountDict[Item][0],CountDict[Item][1])

	sys.stderr.write("Printed output with counts from %s and %s\n"  %(FirstFile,SecondFile))
		