#plot x,y from spectra data where x is wavelength and y is intensity

MyData = read.table("~/Documents/R/30June2011_23Bph8+450Mm+500ul_1_2.txt",header=FALSE,sep="\t")

x=MyData[,1]
y=MyData[,3]

plot(x,y,col="#00000022",cex=.5, xlab="Wavelength",ylab="Intensity")
spy= smooth.spline(x, y, df=11)
lines(x,spy$y,col="blue")

# find the max of the spline...
LambdaMax = median(x[spy$y==max(spy$y)])
lines(c(LambdaMax,LambdaMax),c(min(spy$y),max(spy$y)),col="red")

# par(mfrow=c(1,3))

text(LambdaMax+5,min(spy$y),LambdaMax,adj = c(0,0))

# normalized values

nspy=(spy$y-min(spy$y))*(100/max(spy$y-min(spy$y)))