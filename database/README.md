###MBARI PUBLIC SOFTWARE REPO - DATABASE FOLDER

>All software is provided with the provision that the user will assume risks of its operation. Bugs exist! Use with care and use with files that have been backed up.

# *File descriptions:

* **flickr_search2.py**
	- search flickr for images with certain tags and in certain bounding boxes
* **flickrapi2.py**
    - required for `flickr_search2`
* **vars_image_move.py** 
	- takes images retrieved in bulk by VARS query and reorganizes them into folders
* **vars_retrieve_concept.py**
 	- search MBARI database for dive summaries, samples, and annotations
* **crossref_parse.py**
 	- Using a flat list of refs, try searching for crossref biblio records.


# *Notes on installation

To use `vars_retrieve_concept.py` under OSX, you need to install the MySQL python tools with the following packages (in order):

 * Get the **Command Line Tools** from https://developer.apple.com/downloads/ 
 * Install HomeBrew by pasting this command in a Terminal window: 
 *     `ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"`
 * Install pip with this exact command:
 *     `sudo easy_install pip`
 * `brew install unixodbc`
 * `brew install --with-unixodbc freetds`
 * `sudo pip install pymssql` 

